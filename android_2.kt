import kotlinx.coroutines.*

suspend fun checkWebsite(url: String): Boolean {
    return try {
        val connection = java.net.URL(url).openConnection() as java.net.HttpURLConnection
        connection.requestMethod = "HEAD"
        connection.connectTimeout = 5000
        connection.readTimeout = 5000
        connection.responseCode == java.net.HttpURLConnection.HTTP_OK
    } catch (e: Exception) {
        false
    }
}

fun main() = runBlocking {
    val websites = listOf(
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.github.com",
        "https://www.twitter.com",
        "https://www.instagram.com",
        // Добавьте еще 5 любых сайтов
        "https://www.jetbrains.com",
        "https://www.wikipedia.org",
        "https://www.reddit.com",
        "https://www.linkedin.com",
        "https://www.stackoverflow.com"
    )

    val deferredResults = mutableListOf<Deferred<Pair<String, Boolean>>>()

    for (url in websites) {
        val deferred = async {
            val isAvailable = checkWebsite(url)
            Pair(url, isAvailable)
        }
        deferredResults.add(deferred)
    }

    for (result in deferredResults) {
        val (url, isAvailable) = result.await()
        println("Сайт $url ${if (isAvailable) "доступен" else "недоступен"}")
    }
}
